<?php
require_once('configuration.php');
require_once('functions.php');
require_once('model.php');
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>ShowroomCaptcha</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="css/style.css">
	</head>
	
	<body>
		<div class="validation">
		<?php 
			if(isset($_POST['solution']))
			{
				
				bddConnect();
				
				$id = $_POST['id'];
				
				$captcha = selectSolution($id);
				
				$solution = $captcha['solution'];
				$type = $captcha['type'];
				
				if($solution == $_POST['solution'])
				{
					setPinLow($pins[$outs["LED Machine"]]);
					setPinHigh($pins[$outs["LED Humain"]]);
					setPinLow($pins[$outs["Electro-aimant"]]);
					
					echo "<p>Bravo ! Vous avez prouvé que vous n'étiez pas un robot ! Déverrouillage du panneau !</p>";
					echo "<br /><br /><br /><p><a href='index.php'>Retour à l'accueil</a></p>";
				}
				else
				{
					echo "<p>Vous n'êtes manifestement pas un humain ! Veuillez <a href='captcha.php?type=".$type."' >retenter ce captcha</a> ou en <a href='index.php'> tenter un autre !</a>";
				}
			}
		?>
		</div>	
	</body>

</html>
