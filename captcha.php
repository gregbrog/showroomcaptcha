<?php 
require_once('configuration.php');
require_once('functions.php');
require_once('model.php');
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>ShowroomCaptcha</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="css/style.css">
	</head>
	
	<body>
		<div class="captcha">
		<?php 
			bddConnect();
			
			if(isset($_GET['type']))
			{
				
				$type = $_GET['type'];
				
				if($_GET['type'] == 'operation')
				{
					
					$alea = rand(1,5);
					
					$captcha = selectImage($type, $alea);					
					
					$id = $captcha['id'];
					$src = '/img/captcha/'.$captcha['image'];
					
					?>
						<img alt="captcha_operation" src="<?php echo $src; ?>">
						<form method="post" action="validation.php">
							<p>Veuillez résoudre l'opération : </p>
							<input type='hidden' value='<?php echo $id; ?>' name='id' />
							<input type='text' name='solution' />
							<input type="submit" value='Soumettre !' />
						</form>
					<?php 
				}
				else if($_GET['type'] == 'classique')
				{
					
					$alea = rand(1,15);
											
					$captcha = selectImage($type, $alea);
						
					$id = $captcha['id'];
					$src = '/img/captcha/'.$captcha['image'];
						
					?>
						<img alt="captcha_classique" src="<?php echo $src; ?>">
						<form method="post" action="validation.php">
							<p>Veuillez recopier les lettres que vous voyez à l'écran :</p>
							<input type='hidden' value='<?php echo $id; ?>' name='id' />
							<input type='text' name='solution' />
							<input type="submit" value='Soumettre !' />
						</form>
					<?php 
				}
				else if($_GET['type'] == 'des')
				{
					$alea = rand(1,2);
				
					$captcha = selectImage($type, $alea);
				
					$id = $captcha['id'];
					$src = '/img/captcha/'.$captcha['image'];
				
					?>
						<img alt="captcha_des" src="<?php echo $src; ?>">
						<form method="post" action="validation.php">
							<p>Veuillez additionner les chiffres indiqués par les dés : </p>
							<input type='hidden' value='<?php echo $id; ?>' name='id' />
							<input type='text' name='solution' />
							<input type="submit" value='Soumettre !' />
						</form>
					<?php 
					}
			}
			else
			{
				?>
					<p>Erreur : Vous n'avez pas sélectionné un captcha !</p>
					<p><a href='index.php'>Retour à l'accueil</a>
				<?php 
			}
		?>
		</div>
	</body>

</html>