-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 27 Janvier 2015 à 00:57
-- Version du serveur: 5.5.41
-- Version de PHP: 5.4.36-0+deb7u3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `showroom`
--

-- --------------------------------------------------------

--
-- Structure de la table `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `solution` varchar(100) NOT NULL,
  `alea` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `captcha`
--

INSERT INTO `captcha` (`id`, `image`, `type`, `solution`, `alea`) VALUES
(1, 'captcha1.png', 'classique', 'cralsist', 1),
(2, 'captcha2.png', 'classique', 'flirc', 2),
(3, 'captcha3.png', 'classique', 'unrexc', 3),
(4, 'captcha4.png', 'classique', 'valite', 4),
(5, 'captcha5.png', 'classique', 'dultne', 5),
(6, 'captcha6.png', 'classique', 'sonated', 6),
(7, 'captcha7.png', 'classique', 'plicrom', 7),
(8, 'captcha8.png', 'classique', 'unbachar', 8),
(9, 'captcha9.png', 'classique', 'aphxleci', 9),
(10, 'captcha10.png', 'classique', 'tegunt', 10),
(11, 'captcha11.png', 'classique', 'ejujle', 11),
(12, 'captcha12.png', 'classique', 'unnedou', 12),
(13, 'captcha13.png', 'classique', 'vedsho', 13),
(14, 'captcha14.png', 'classique', 'trapperb', 14),
(15, 'captcha15.png', 'classique', 'wrans', 15),
(16, 'des_1.jpg', 'des', '8', 1),
(17, 'des_2.jpg', 'des', '10', 2),
(18, 'operation_1.jpg', 'operation', '9', 1),
(19, 'operation_2.jpg', 'operation', '7', 2),
(20, 'operation_3.jpg', 'operation', '30', 3),
(21, 'operation_4.jpg', 'operation', '18', 4),
(22, 'operation_5.jpg', 'operation', '4', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
