<?php 
require_once('configuration.php');
require_once('functions.php');

//Remise en position du syst�me

setPinHigh($pins[$outs["LED Machine"]]);
setPinLow($pins[$outs["LED Humain"]]);
setPinHigh($pins[$outs["Electro-aimant"]]);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>ShowroomCaptcha</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

<table class="captchaTab">
	<tr>
		<th>Captcha</th>
		<th>Image</th>
		<th>Go !</th>
	</tr>
	<tr>
		<td>Captcha Classique</td>
		<td><img alt="Captcha" src="img/captcha.jpg"></td>
		<td><a href="captcha.php?type=classique"><img alt="Go !" src="img/fleche.png"></a></td>
	</tr>
	<tr>
		<td>Captcha à Dés</td>
		<td><img alt="Dés" src="img/des.png"></td>
		<td><a href="captcha.php?type=des"><img alt="Go !" src="img/fleche.png"></a></td>
	</tr>
	<tr>
		<td>Captcha à Opération</td>
		<td><img alt="Opération" src="img/operation.jpg"></td>
		<td><a href="captcha.php?type=operation"><img alt="Go !" src="img/fleche.png"></a></td>
	</tr>
</table>

<script src="js/jquery.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>